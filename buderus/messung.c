/*
 * messung.c
 *
 *  Created on: 13.04.2019
 *      Author: googy
 */

#include <avr/interrupt.h>
#include "messung.h"
#include "../timebase.h"

const uint8_t lut[] = {
        109,    // 512
        84,     // 1024
        69,     // 1536
        58,     // 2048
        50,     // 2560
        44,     // 3072
        38,     // 3584
        33,     // 4096
        29,     // 4608
        25,     // 5120
        21,     // 5632
        18,     // 6144
        15,     // 6656
        13      // 7168
};

static uint8_t state = 3;
uint32_t timecounter = 0;
// Messergebnis ADC
static volatile uint16_t mval;
static volatile uint8_t flag;

// Timer1 für ADC Messung
void icp_init() { // ATmega328p
    mval = 0;
    TCCR1B = 0;                             // stop Timer
    TCNT1L = 0;                             // reset counter value
    TCNT1H = 0;                             // reset counter value
    TIFR1 = (1 << 5) | (1 << 0);            // reset interrupt flags
    TIMSK1 |= (1<<ICIE1) | (1 << TOIE1);    // Input-capture interrupt aktivieren

    TCCR1A = 0;                             // normal mode, keine PWM AusgÃ¤nge
    TCCR1C = 0;                             // kein Force Output Compare
    TCCR1B = (1 << ICNC1) | (0 << ICES1)    // noise canceler, fallende Flanke auswählen
            | (1 << CS10) | (1 << CS11) | (0 << CS12);    // prescaler 64, damit wird der Timer gestartet
}

//  Flanke an ICP pin, Ergebnis liegt vor
ISR(TIMER1_CAPT_vect) {
    TCCR1B = 0;         // timer stoppen, damit kein overflow interrupt generiert wird
    TIMSK1 &= ~(1<<ICIE1);

    // Timer 1 Input Capture Wert auslesen
    mval = ICR1L;
    mval |= (ICR1H << 8);
    // Timer Wert zurücksetzen
    ICR1L = 0;
    ICR1H = 0;
    // signalisiere erfolgreiche Messung
    flag = OK;
}

// Overflow ohne ein Event an ICP pin vorher, das bedeutet einen Fehler
ISR(TIMER1_OVF_vect){
    TCCR1B = 0; // timer stoppen
    // signalisiere fehlgeschlagene Messung
    flag = ERROR;
}


uint8_t messung_done() {
    if (flag != INIT)
        return 0xFF;
    else
        return 0x00;
}

uint8_t messung_t_erg() {
    return lut[(mval >> 9) - 1] - (((lut[(mval >> 9) - 1] - lut[mval >> 9]) * (mval - (mval >> 9 << 9))) >> 9);
//    return lut[mval >> 9] + (((lut[(mval >> 9) - 1] - lut[mval >> 9]) * (mval - (mval >> 9 << 9))) >> 9);
}

void messung_init(uint8_t channel) {
    // ADC als eingang
    DDRB &= ~(1 << PB0);    // ICP
    PORTB |= (1 << PB0);    // ICP pull up

    // Pins zur Auswahl des zu messenden Kanals
    DDRC |= (1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3);  // S0 S1 S2 T5
    // Kanal auswählen
    PORTC &= ~((1 << S2) | (1 << S1) | (1 << S0));      // reset Kanalauswahl
    PORTC |= (channel & 0b111);                         // wähle Messkanal
    state = 0;
    flag = INIT;
}

void messung_compute() {
    switch (state){
    case 0: // initialisiere Messung
        // Kondensator entladen
        PORTC |= (1 << T5);     // Kondensator Entladetransistor T5 einschalten

        stoptimer_init(&timecounter);
        state = 1;
        break;
    case 1: // starte Messung
        // warte bis der Sample Kondensator vollst�ndig entladen ist, dauert grob 200ms
        if (!stoptimer_expired(&timecounter, 200)) {
            break;
        }

        // Messung starten
        icp_init();             // Timer starten mit ICP
        PORTC &= ~(1 << T5);    // Messung starten, Vergleichskondensator laden, sobald dieser Ã¼ber Messwert ist, wird ICP low gezogen
        state = 2;
        break;
    case 2: // warte auf Ergebnis
        // warte bis Messung abgeschlossen ist
        if (flag == INIT) {
            break;
        }

        // Messung erfolgreich, Flanke an ICP
        if (flag == OK) {
            state = 3;
            break;
        }

        // Messung nicht erfolgreich, Timer1 overflow
        if (flag == ERROR) {
            state = 3;
            break;
        }
        break;
    case 3: // idle
        break;
    default: state = 0;
    }
}
