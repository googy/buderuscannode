/*
 * sensoren.c
 *
 *  Created on: 14.04.2019
 *      Author: googy
 */
#include <avr/io.h>
#include "sensoren.h"
#include "messung.h"

static uint8_t messtate = 9;

void buderus_sensoren() {

    switch (messtate) {
        case 0: // Diesel-Kessel
            if (!messung_done())
                messung_compute();
            else {
                values.kesseltemperatur = messung_t_erg();
                messung_init(1);
                messtate = 1;
            }
            break;

        case 1: // Außenfühler (abgeschaltet)
            if (!messung_done())
                messung_compute();
            else {
                values.aussenfuehler = messung_t_erg();
                messung_init(2);
                messtate = 2;
            }
            break;

        case 2: // Warmwasser
            if (!messung_done())
                messung_compute();
            else {
                values.brauchwasser = messung_t_erg();
                messung_init(3);
                messtate = 3;
            }
            break;

        case 3: // HK2 FM241 Modul anwesend
            if (!messung_done())
                messung_compute();
            else {
                values.HK2modul = messung_t_erg();
                messung_init(5);
                messtate = 5;
            }
            break;

        case 5: // Brenner Status
            if (!messung_done())
                messung_compute();
            else {
                values.brenner = messung_t_erg();
                messung_init(6);
                messtate = 6;
            }
            break;

        case 6: // HK2 Temperatur
            if (!messung_done())
                messung_compute();
            else {
                values.vorlauf_hk2 = messung_t_erg();
                messung_init(0);
                messtate = 0;
            }
            break;

        case 9:
            messung_init(0);
            messtate = 0;
            break;

        default:
            messtate = 0;
        }
}
